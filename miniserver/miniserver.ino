/* Create a WiFi access point and provide a web server on it. */

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <Ticker.h>

#include "html_cnt.h"

#ifndef APSSID
#define APSSID "Mokamobile"
#define APPSK  "thereisnospoon"
#endif

Ticker timer;
int measTime = 0;

/* Set these to your desired credentials. */
const char *ssid = APSSID;
const char *password = APPSK;

ESP8266WebServer server(80);

float val_cur = 1.23;
float val_min = 0.12;
float val_max = 2.34;

String msg = "toto was here";

/* Just a little test message.  Go to http://192.168.4.1 in a web browser
   connected to this access point to see it.
*/
void handleRoot() {
  Serial.println("serving root");
  server.send(200, "text/html", gen_root_html(msg, val_cur));
}

void handleSettings() {
  Serial.println("serving settings");
  Serial.print("submitted: ");
  Serial.println(server.hasArg("submit"));
  if (server.hasArg("submit")) {
    val_min = String(server.arg("val_min")).toFloat();
    val_max = String(server.arg("val_max")).toFloat();
  }

  server.send(200, "text/html", gen_settings_html(val_max, val_min));
}

// ISR to Fire when Timer is triggered
void IRAM_ATTR onTime() {
  measTime = 0;
}

void setup() {
  delay(1000);
  Serial.begin(115200);

  //Initialize Ticker every 0.5s
  timer1_attachInterrupt(onTime); // Add ISR Function
  timer1_enable(TIM_DIV256, TIM_EDGE, TIM_SINGLE);
  /* Dividers:
    TIM_DIV1 = 0,   //80MHz (80 ticks/us - 104857.588 us max)
    TIM_DIV16 = 1,  //5MHz (5 ticks/us - 1677721.4 us max)
    TIM_DIV256 = 3  //312.5Khz (1 tick = 3.2us - 26843542.4 us max)
    Reloads:
    TIM_SINGLE  0 //on interrupt routine you need to write a new value to start the timer again
    TIM_LOOP  1 //on interrupt the counter will start with the same value again
  */

  Serial.println();
  Serial.print("Configuring access point...");
  /* You can remove the password parameter if you want the AP to be open. */
  WiFi.softAP(ssid, password);

  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  server.on("/", handleRoot);
  server.on("/settings.html", HTTP_GET, handleSettings);
  server.on("/settings.html", HTTP_POST, handleSettings);
  server.onNotFound([]() {
    server.send(404, "text/plain", "404: Not found");
  });
  server.begin();
  Serial.println("HTTP server started");
}

void loop() {
  // actually perform custom management other than serving webpage
  if (measTime == 0) {
    delay(300);
    val_cur = (float)random((long)(val_min * 100), (long)(val_max * 100)) / 100.;
    Serial.println(String("measured:") + String(val_cur, 2) + String(" (min, max)") + String(val_min, 2) + String(", ") + String(val_max, 2));
    measTime = 1;
    timer1_write(1000000);//3s
  }

  server.handleClient();
}
