#include "sensor_ds18b20.h"

#include "measure.h"

#define SAMPLE_NB 400

uint8_t temp_addr[] = { 40, 37, 63, 7, 214, 1, 60, 63 };
DS18B20 ds(5);  // (D1);

float temp_cur = 0.;
uint16_t mem_length = SAMPLE_NB;
float temp_mem[SAMPLE_NB];


void measure::setup() {
  if (ds.search() == 0) {
#if (DEBUG == 1)
    Serial.println("Thermo sensor one wire, did not acknowledge! Freezing!");
#endif
    pinMode(LED_BUILTIN, OUTPUT);
    for (uint8_t i = 0; i < 10; i++) {
      digitalWrite(LED_BUILTIN, HIGH);
      delay(100);
      digitalWrite(LED_BUILTIN, LOW);
      delay(200);
    }
    // ESP.restart();
  }

  // ds.select(temp_addr);
#if (DEBUG == 1)
  Serial.println("sensor selected");
#endif

  for (uint16_t i = 0; i < SAMPLE_NB; i++) {
    temp_mem[i] = 0;
  }

}


void measure::sample() {
  temp_cur = 3.21; // ds.getTempC();
#if (DEBUG == 1)
  Serial.print(temp_cur);
  Serial.println(" [°C] measured");
#endif
  for (uint16_t i = SAMPLE_NB - 1; i > 0; i--) {
    temp_mem[i] = temp_mem[i - 1];
  }
  temp_mem[0] = temp_cur;

}
