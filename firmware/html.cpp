#include <html_server.h>

#include "html.h"
#include "interact.h"
#include "measure.h"

HtmlServer server(80);

void html::setup() {
  server.on("/", html::handle_root);
  server.on("/style.css", html::handle_style);
  server.on("/favicon.ico", html::handle_favicon);
  server.on("/settings.html", HTTP_GET, html::handle_settings);
  server.on("/settings.html", HTTP_POST, html::handle_settings);
  server.on_not_found(html::handle_not_found);
  server.begin();

}

void html::loop() {
  server.handleClient();
}


String html::processor(const String& key) {
  if (key == "temp_cur") {
    return String(temp_cur, 2);
  }
  else if (key == "motor_state") {
    String state_descr;
    switch (motor_state) {
      case 0:
        state_descr = "stopped";
        break;
      case 1:
        state_descr = "running";
        break;
      case 2:
        state_descr = "resting";
        break;
      default:
        state_descr = "fault";
        break;
    }
    return state_descr;
  }
  else if (key == "svg") {
// #if (DEBUG == 1)
//   Serial.println(svg);
// #endif
    return svg;
  }
  else if (key == "temp_max") {
    return String(t_th_max, 2);
  }
  else if (key == "temp_min") {
    return String(t_th_min, 2);
  }
  else if (key == "run_max") {
    return String(run_max);
  }
  else if (key == "rest_min") {
    return String(rest_min);
  }
  return "Key not found";
}


void html::handle_root() {
#if (DEBUG == 1)
  Serial.println("serving root");
#endif
  if (server.method() == HTTP_POST) {
#if (DEBUG == 1)
    Serial.println("POST to root.html");
    String message = "POST form was:\n";
    for (uint8_t i = 0; i < server.args(); i++) {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    Serial.println(message);
#endif
  }

  server.process_and_send("/root.html", html::processor);
}

void html::handle_favicon() {
#if (DEBUG == 1)
  Serial.println("serving favicon.ico");
#endif
  server.send_plain("favicon");
}


void html::handle_style() {
#if (DEBUG == 1)
  Serial.println("serving style.css");
#endif
  server.send("/style.css", "text/css");
}


void html::handle_not_found() {
#if (DEBUG == 1)
  Serial.println("serving page not found: " + server.uri());
#endif
  server.send("/not_found.html", "text/html");
}


void html::handle_settings() {
#if (DEBUG == 1)
  Serial.println("serving settings");
#endif

  if (server.method() == HTTP_POST) {
#if (DEBUG == 1)
    Serial.println("POST to root.html");
    String message = "POST form was:\n";
    for (uint8_t i = 0; i < server.args(); i++) {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    Serial.println(message);
#endif
  }
   for (uint8_t i = 0; i < server.args(); i++) {
      if (server.argName(i) == "temp_max") {
        t_th_max = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "temp_min") {
        t_th_min = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "run_max") {
        run_max = server.arg(i).toInt();
      }
      else if (server.argName(i) == "rest_min") {
        rest_min = server.arg(i).toInt();
      }
    }

  interact::save_cfg_file();
  
  server.process_and_send("/settings.html", html::processor);
}
