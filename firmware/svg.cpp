#include "svg.h"

uint16_t svg_width = 840;  // [pix]
uint16_t svg_height = 440;  // [pix]
uint16_t svg_bbox[4] = {20, 420, 800, 400};  // [pix] graph bounding box

float t_max = 15.;
float t_min = 0.;

float time_to_svgx(uint16_t index, uint16_t nb_samples) {
  return svg_bbox[0] + svg_bbox[2] * (1. - (float)index / nb_samples) ; // present time is on right border of graph
}

float temp_to_svgy(float temp) {
  return svg_bbox[1] - (temp - t_min) / (t_max - t_min) * svg_bbox[3];
}

String gen_graph(float* temp_mem, uint16_t nb_samples, float t_th_min, float t_th_max) {
  uint16_t i;
  uint16_t t_ind;

  float x;
  float y;
  String svg = "";

  char buffer[300];

  // find t_min and t_max
  t_min = min(t_th_min, (float)0.);
  t_max = max(t_th_max, (float)0.);
  for (i = 0; i < nb_samples; i++) {
    t_min = min(t_min, temp_mem[i]);
    t_max = max(t_max, temp_mem[i]);
  }

  t_min -= 0.1;
  t_max += 0.1;

  // svg header
  sprintf(buffer,
          "<svg height=\"%dpx\" version=\"1.1\" viewBox=\"0 0 %d %d\" width=\"%dpx\" xmlns=\"http://www.w3.org/2000/svg\">\n",
          svg_height, svg_width, svg_height, svg_width);

  svg += String(buffer);

  // bounding box
  sprintf(buffer,
          "\t<path d='M %d %d h %d v %d h %d v %d'\n",
          svg_bbox[0], svg_bbox[1], svg_bbox[2], -1 * (int)svg_bbox[3], -1 * (int)svg_bbox[2], svg_bbox[3]
         );
  svg += String(buffer);
  svg += String("\t      style='fill:none;stroke:#000000;stroke-linecap:square;stroke-width:1;'/>\n");
  
  // xlabel
  y = svg_bbox[1];
  for (t_ind = 0; t_ind < nb_samples; t_ind = t_ind + 90) {  // one mark every 15 minutes see timer1_write below
    sprintf(buffer,
            "\t<path d='M %.1f %.1f v 10'\n",
            time_to_svgx(t_ind, nb_samples), y
           );
    svg += String(buffer);
    svg += String("\t      style='fill:none;stroke:#000000;stroke-linecap:square;stroke-width:1;'/>\n");

    sprintf(buffer,
            "\t<text x='%.1f' y='%.1f' font-size='12'>%.0f</text>\n",
            time_to_svgx(t_ind, nb_samples) - 10, y + 20., t_ind / -6.
           );
    svg += String(buffer);
  }

  // ylabel
  int16_t y_labels[5];
  uint8_t nb_y_labels = 0;
  float y_tests[] =  {t_min + (float)0.1, t_th_min, 0, t_th_max, t_max - (float)0.1};
  bool already_exists;
  uint8_t ii;
  int16_t y_test_cur;

  for (i = 0; i < 5; i++) {
    y_test_cur = (int16_t)(y_tests[i] * 10);
    already_exists = false;
    for (ii = 0; ii < nb_y_labels; ii++) {
      if (y_labels[ii] == y_test_cur) {
        already_exists = true;
      }
    }

    if (!already_exists) {
      y_labels[nb_y_labels] = y_test_cur;
      nb_y_labels ++;
    }
  }

  x = svg_bbox[0];
  for (i = 0; i < nb_y_labels; i++) {
    sprintf(buffer,
            "\t<path d='M %.1f %.1f h 800'\n",
            x, temp_to_svgy(y_labels[i] / 10.)
           );
    svg += String(buffer);
    if (y_labels[i] == 0) {
      svg += String("\t      style='fill:none;stroke:#aa0000;stroke-linecap:square;stroke-width:1;stroke-dasharray:4;'/>\n");
    } else {
      svg += String("\t      style='fill:none;stroke:#000000;stroke-linecap:square;stroke-width:1;stroke-dasharray:4;'/>\n");
    }

    sprintf(buffer,
            "\t<text x='%.1f' y='%.1f' font-size='12'>%.1f</text>\n",
            x - 20, temp_to_svgy(y_labels[i] / 10.) + 5, y_labels[i] / 10.
           );
    svg += String(buffer);
  }

  // graph of temperatures
  sprintf(buffer,
          "\t<path d='M %.0f %.0f",  // \n",
          time_to_svgx(0, nb_samples), temp_to_svgy(temp_mem[0])
         );
  svg += String(buffer);

  for (i = 1; i < nb_samples; i++) {
    sprintf(buffer,
            " L %.0f %.0f",  // \n",  // \t          ,
            time_to_svgx(i, nb_samples), temp_to_svgy(temp_mem[i])
           );
    svg += String(buffer);
  }

  svg = svg + String("\t'\n style='fill:none;stroke:#1f77b4;stroke-linecap:square;stroke-width:1.5;'/>\n");

  // svg footer
  svg += String("</svg>\n");

  return svg;
}
