#ifndef SVG_H
#define SVG_H

#include <Arduino.h>

float time_to_svgx(uint16_t, uint16_t);
float temp_to_svgy(float);
String gen_graph(float*, uint16_t, float, float);

#endif //  SVG_H
