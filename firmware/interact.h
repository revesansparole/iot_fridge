#ifndef INTERACT_H
#define INTERACT_H

#include "settings.h"

extern float t_th_min;
extern float t_th_max;

extern uint16_t run_max;
extern uint16_t rest_min;
extern uint8_t motor_state;
extern uint16_t motor_timer;

extern String svg;

namespace interact {

void setup();
void perform();

void clear_display();
void display_current();

void load_cfg_file();
void save_cfg_file();

}  // namespace interact

#endif //  INTERACT_H
