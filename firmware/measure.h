#ifndef MEASURE_H
#define MEASURE_H

#include "settings.h"

extern float temp_cur;
extern uint16_t mem_length;
extern float temp_mem[];

namespace measure {

void setup();
void sample();

}  // namespace measure

#endif //  MEASURE_H
