#ifndef HTML_H
#define HTML_H

#include "settings.h"

namespace html {

void setup();
void loop();

String processor(const String& key);
void handle_root();

void handle_favicon();
void handle_style();
void handle_not_found();
void handle_settings();

}  // namespace html

#endif //  HTML_H
