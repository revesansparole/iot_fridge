#include <ESP8266WiFi.h>
#include <Ticker.h>


#include "html.h"
#include "interact.h"
#include "measure.h"


Ticker timer;
bool time_to_sample = true;

const char *ssid = "Mokamobile";
const char *password = "thereisnospoon";


/* Go to http://192.168.4.1 in a web browser
   connected to this access point to see it.
*/

// ISR to Fire when Timer is triggered
void IRAM_ATTR onTime() {
  time_to_sample = true;
}

void setup() {
#if (DEBUG == 1)
  Serial.begin(115200);
  Serial.println("setup!");
#endif

  //Initialize Ticker every 0.5s
  timer1_attachInterrupt(onTime); // Add ISR Function
  timer1_enable(TIM_DIV256, TIM_EDGE, TIM_SINGLE);
  /* Dividers:
    TIM_DIV1 = 0,   //80MHz (80 ticks/us - 104857.588 us max)
    TIM_DIV16 = 1,  //5MHz (5 ticks/us - 1677721.4 us max)
    TIM_DIV256 = 3  //312.5Khz (1 tick = 3.2us - 26843542.4 us max)
    Reloads:
    TIM_SINGLE  0 //on interrupt routine you need to write a new value to start the timer again
    TIM_LOOP  1 //on interrupt the counter will start with the same value again
  */

    measure::setup();
    interact::setup();

#if (DEBUG == 1)
  Serial.println();
  Serial.print("Configuring access point...");
#endif

  WiFi.softAP(ssid, password);
#if (DEBUG == 1)
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
#endif

  html::setup();
#if (DEBUG == 1)
  Serial.println("HTTP server started");
#endif
}

void loop() {
  if (time_to_sample) {
    interact::clear_display();
    measure::sample();
    interact::perform();

    // reinit timer
    time_to_sample = false;
    timer1_write(3333333);  // 1e6 corresponds to 3s
  }

  interact::display_current();
  html::loop();
}
