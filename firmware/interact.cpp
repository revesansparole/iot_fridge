#include <ArduinoJson.h>
#include <LittleFS.h>

#include "display_3461bs.h"

#include "interact.h"
#include "measure.h"
#include "svg.h"

const int relay = 4; // D2;
Display3461BS disp(12, 13, 14); // (D6, D7, D5);

float t_th_min;
float t_th_max;

uint16_t run_max;
uint16_t rest_min;
uint8_t motor_state;
uint16_t motor_timer;

String svg;

void interact::setup() {
LittleFS.begin();
  interact::load_cfg_file();
  motor_state = 0;
  motor_timer = 0;

  pinMode(relay, OUTPUT);
  disp.begin();
}

void interact::perform() {
// update svg curve
    svg = gen_graph(temp_mem, mem_length, t_th_min, t_th_max);

// manage motor
  switch (motor_state) {
    case 0:  // stopped
      if (temp_cur > t_th_max) {
#if (DEBUG == 1)
        Serial.println("turning compressor ON");
#endif
        digitalWrite(relay, HIGH);
        motor_state = 1;
        motor_timer = 0;
      }
      break;
    case 1:  // running
      motor_timer += 10;  // value linked to timer1_write below
      if (temp_cur < t_th_min or motor_timer >= run_max) {
#if (DEBUG == 1)
        Serial.println("turning compressor OFF");
#endif
        digitalWrite(relay, LOW);
        motor_state = 2;
        motor_timer = 0;
      }
      break;
    case 2:  // resting
      motor_timer += 10;  // value linked to timer1_write below
      if (motor_timer >= rest_min) {
#if (DEBUG == 1)
        Serial.println("compressor rested");
#endif
        motor_state = 0;
        motor_timer = 0;
      }
      break;
    default:  // not supposed to happen
      break;
  }
}


void interact::clear_display() {
  disp.clear();
}


void interact::display_current() {
  disp.display(temp_cur);
}


void interact::load_cfg_file() {
  StaticJsonDocument<128> doc;
  File fhr = LittleFS.open("config.json", "r");
  deserializeJson(doc, fhr);
  fhr.close();

#if (DEBUG == 1)
  Serial.println("Loaded config:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif

  t_th_max = doc["t_th_max"];
  t_th_min = doc["t_th_min"];
  run_max = (uint16_t)doc["run_max"];
  rest_min = (uint16_t)doc["rest_min"];
}


void interact::save_cfg_file() {
  StaticJsonDocument<128> doc;

  doc["t_th_max"] = t_th_max;
  doc["t_th_min"] = t_th_min;
  doc["run_max"] = run_max;
  doc["rest_min"] = rest_min;

  File fhw = LittleFS.open("config.json", "w");
  serializeJsonPretty(doc, fhw);
  fhw.close();

#if (DEBUG == 1)
  Serial.println("Saved config:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif
}
